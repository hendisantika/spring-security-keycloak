package com.hendisantika.springsecuritykeycloak;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-security-keycloak
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/08/18
 * Time: 07.52
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ProductService {
    public List<String> getProducts() {
        return Arrays.asList("iPad", "iPod", "iPhone", "iMac", "Mac Book Pro", "Mac Book Air");
    }

}

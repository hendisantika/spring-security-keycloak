# Spring Boot - Keycloak example

#### Prerequisite

Follow the instructions on [Keycloak website](https://www.keycloak.org/documentation.html) on how to setup Keycloak

Next navigate to the directory of the cloned repository and run:

`mvnw clean spring-boot:run`

### Run Keycloak via Docker Compose

```shell
version: '3'

volumes:
  postgres_data:
      driver: local

services:
  postgres:
      image: postgres
      volumes:
        - postgres_data:/var/lib/postgresql/data
      environment:
        POSTGRES_DB: keycloak
        POSTGRES_USER: keycloak
        POSTGRES_PASSWORD: password
  keycloak:
      image: quay.io/keycloak/keycloak:legacy
      environment:
        DB_VENDOR: POSTGRES
        DB_ADDR: postgres
        DB_DATABASE: keycloak
        DB_USER: keycloak
        DB_SCHEMA: public
        DB_PASSWORD: password
        KEYCLOAK_USER: admin
        KEYCLOAK_PASSWORD: Pa55w0rd
        # Uncomment the line below if you want to specify JDBC parameters. The parameter below is just an example, and it shouldn't be used in production without knowledge. It is highly recommended that you read the PostgreSQL JDBC driver documentation in order to use it.
        #JDBC_PARAMS: "ssl=true"
      ports:
        - 8080:8080
      depends_on:
        - postgres
```

Open your favorite browser: http://localhost:8081

Home Page

![Home Page](img/home.png "Home Page")

Keycloak Login Page

![Keycloak Login Page](img/login.png "Keycloak Login Page")

Product Page

![Product Page](img/list.png "Product Page")
